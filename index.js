import express from 'express';
import cors from 'cors';
import 'dotenv/config';
const db = require('./queries')
const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get('/', (req, res) => {
  return res.send("Hello use /users in url to see user data");
});
app.get('/users', db.getUsers)
app.post('/users', db.createUser)
app.get('/users/:id', db.getUserById)
app.delete('/users/:id', db.deleteUser)
app.put('/users/:id', db.updateUser)


app.listen(process.env.PORT, () =>
  console.log(`Example app listening on port ${process.env.PORT}!`),
);