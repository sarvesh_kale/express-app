const Pool = require('pg').Pool
const pool = new Pool({
  user: 'deqode',
  host: 'localhost',
  database: 'employee_data',
  password: '12345',
  port: 5432,
})

const getUsers = (request, response) => {
  pool.query('SELECT * FROM employee ORDER BY user_id ASC', (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const getUserById = (request, response) => {
  const id = parseInt(request.params.id)

  pool.query('SELECT * FROM employee WHERE user_id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const createUser = (request, response) => {
  const { name, dob, email, phone_number, gender, description } = request.body

  pool.query('INSERT INTO employee (name, dob, email, phone_number, gender, description) VALUES ($1, $2, $3, $4, $5, $6)', [name, dob, email, phone_number, gender, description], (error, results) => {
    if (error) {
      throw error
    }
    response.status(201).send(`User Added`)
  })
}

const updateUser = (request, response) => {
  const id = parseInt(request.params.id)
  const { name, dob, email, phone_number, gender, description } = request.body

  pool.query(
    'UPDATE employee SET name=$1, dob=$2, email=$3, phone_number=$4, gender=$5, description=$6 WHERE user_id = $7',
    [name, dob, email, phone_number, gender, description, id],
    (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).send(`User modified with ID: ${id}`)
    }
  )
}

const deleteUser = (request, response) => {
  const id = parseInt(request.params.id)

  pool.query('DELETE FROM employee WHERE user_id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).send(`User deleted`)
  })
}

module.exports = {
  getUsers,
  getUserById,
  createUser,
  updateUser,
  deleteUser,
}